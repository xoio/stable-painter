import type {Component} from 'solid-js';
import {createEffect, createResource, createSignal, onMount} from "solid-js";
import Sketch from "./sketch";
import constants from "./constants";

export default function (props: any) {

    // @ts-ignore
    const {state} = props

    let canvas_container
    let p: any
    let x = 0
    let y = 0

    let initial_image: any = null

    let prompt_ref

    onMount(async () => {

        //@ts-ignore
        p = new window.p5(Sketch, canvas_container)
        p.draw = draw

        // init the setup
        if (!state.setup) {
            await setup_prompt();

        }
    })

    /**
     * Sets up the prompt and downloads the initial image to use for the coloring.
     */
    async function setup_prompt() {
        let img_res = await fetch(`${constants.get_setup()}?prompt=${state.prompt}`, {
            mode: "cors"
        })

        let img = await img_res.blob()
        let i = URL.createObjectURL(img)

        initial_image = await new Promise((res, rej) => {
            let img = new Image()
            img.src = i
            img.onload = () => {
                res(buildP5Image(img))
            }
        })

        initial_image.loadPixels()
        state.setup = true
    }

    /**
     * Grabs current contents of canvas to generate another diffusion image based off of the contents.
     */
    function grab_canvas() {

        console.log("Processing canvas")
        let canvas = canvas_container.children[0]
        let data = canvas.toDataURL()

        // remove the data specification
        let str = data.split(",")[1]

        let form = new FormData()
        form.append("image", str)

        fetch('http://localhost:5000/debug', {
            method: 'POST',
            mode: 'cors',
            body: form
        }).then(res => res.blob()).then(d => {

            let i = URL.createObjectURL(d)
            let img = new Image()
            img.src = i
            img.onload = () => {
                let i = buildP5Image(img)
                p.image(i, 0, 0)
            }

        })
    }

    // constructs an image based on the specified content that makes it usable within p5
    function buildP5Image(img: HTMLImageElement) {
        const pImg = new window.p5.Image(1, 1, p);
        pImg.width = pImg.canvas.width = img.width;
        pImg.height = pImg.canvas.height = img.height;

        pImg.drawingContext.drawImage(img, 0, 0);
        pImg.modified = true;
        return pImg
    }

    // Run the render loop
    function draw() {
        x = p.mouseX
        y = p.mouseY


        if (initial_image !== null) {
            if (p.mouseIsPressed) {
                let off = (y * 512 + x) * 4;
                let col = [
                    initial_image.pixels[off],
                    initial_image.pixels[off + 1],
                    initial_image.pixels[off + 2],
                    initial_image.pixels[off + 3]
                ]
                p.fill(col[0], col[1], col[2])
                p.ellipse(x, y, 20, 20)
            }
        }
    }

    function submit(e: any) {
        e.preventDefault()

        grab_canvas()

    }

    /**
     * Update the prompt used to generate the next image.
     */
    async function update_prompt() {

        setTimeout(async () => {

            console.log(prompt_ref)
            await fetch(`${constants.get_prompt_update()}?prompt=${prompt_ref.value !== "" ? prompt_ref.value : state.prompt}`, {
                mode: "cors"
            })
        })
    }

    return (

        <>
            <section id={"surface"} ref={canvas_container}>
            </section>

            <div id={"prompt"}>
                <form onsubmit={submit}>
                    <input ref={prompt_ref} type={"text"} placeholder={state.prompt}/>
                    <button onclick={update_prompt}>Update Prompt</button>
                    <input type="submit" value={state.setup ? "Update Canvas" : "Submit"}/>
                </form>
            </div>
        </>
    );
}