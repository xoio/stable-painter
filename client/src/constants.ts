export default {
    server: "http://localhost:5000",
    setup:"setup",

    update_prompt:"update_prompt",

    get_setup(){
        return `${this.server}/${this.setup}`
    },

    get_prompt_update(){
        return `${this.server}/${this.update_prompt}`
    }

}