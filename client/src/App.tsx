import type {Component} from 'solid-js';

import styles from './App.module.css';
import Drawer from "./drawer";
import {onCleanup} from "solid-js";

const App: Component = () => {

    // check local storage for general state
    let state = localStorage.getItem("STABLE_PAINTER")

    if (!state) {
        //@ts-ignore
        state = {
            prompt: "A rocket ship launching from a swimming pool"
        }
    }

    onCleanup(() => {
        // save state to localStorage
        localStorage.setItem("STABLE_PAINTER", JSON.stringify(state))
    })

    return (
        <div class={styles.header}>
            <Drawer state={state}/>
        </div>
    );
};

export default App;
