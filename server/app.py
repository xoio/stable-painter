import base64
import io
import os
from base64 import encodebytes
import werkzeug.exceptions
from PIL import Image

import warnings
from dotenv import load_dotenv
from flask import Flask, request, jsonify, send_file, abort, json
from PIL import Image
from stability_sdk import client
import stability_sdk.interfaces.gooseai.generation.generation_pb2 as generation

load_dotenv()

###### STABILITY AI SETUP ###################

# Our Host URL should not be prepended with "https" nor should it have a trailing slash.
os.environ['STABILITY_HOST'] = 'grpc.stability.ai:443'

app = Flask(__name__)
os.environ['STABILITY_KEY'] = os.getenv("STABILITY_AI")

# Set up our connection to the API.
stability_api = client.StabilityInference(
    key=os.environ['STABILITY_KEY'],  # API Key reference.
    verbose=True,  # Print debug messages.
    engine="stable-diffusion-v1-5",  # Set the engine to use for generation. For SD 2.0 use "stable-diffusion-v2-0".
    # Available engines: stable-diffusion-v1 stable-diffusion-v1-5 stable-diffusion-512-v2-0 stable-diffusion-768-v2-0
    # stable-diffusion-512-v2-1 stable-diffusion-768-v2-1 stable-inpainting-v1-0 stable-inpainting-512-v2-0
)

# Settings for the generation.
settings = {
    'seed': 89222675,
    'steps': 30,
    'cfg_scale': 8,
    'width': 512,
    'height': 512,
    'sampler': generation.SAMPLER_K_DPMPP_2M,
    'prompt': "A rocket ship launching from a swimming pool",
    'init_image': None
}


######### HELPERS #################

# Checks to see if artifact might have run into the image filter. Return True if we did, otherwise false if not.
def check_artifact(artifact):
    if artifact.finish_reason == generation.FILTER:
        warnings.warn("Unable to process image. Filter tripped")
        return True
    else:
        return False


# Converts PIL image to something serve-able by flask
def serve_image(pil_img):
    img_io = io.BytesIO()
    pil_img.save(img_io, 'JPEG', quality=70)
    img_io.seek(0)
    return send_file(img_io, mimetype="image/jpeg")


# encodes image as base64
def encode_image(pil_img):
    img_io = io.BytesIO()
    pil_img.save(img_io, 'JPEG', quality=70)
    return encodebytes(img_io.getvalue()).decode("ascii")


# decodes base 64 image into something PIL can use
def decode_b64_image(img):
    return Image.open(io.BytesIO(base64.b64decode(img)))


# https://platform.stability.ai/docs/features/image-to-image
######### ROUTES #################
@app.route('/')
def hello_world():  # put application's code here
    return 'Hello World!'


# Error handler iif there's no prompt.
@app.errorhandler(werkzeug.exceptions.BadRequest)
def handle_no_prompt(e):
    response = e.get_response()
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })

    response.content_type = "application/json"
    return response


# Sets things up by generating an image based on the prompt and sending that back to be used
# as the initial "paint". Prompt should be provided as query parameter.
@app.route('/setup', methods=["GET"])
def setup():
    if request.method == "GET":
        args = request.args
        if args.get("prompt") == None:
            abort(400, "No prompt provided")
        else:

            answers = stability_api.generate(
                prompt=args.get("prompt"),
                seed=settings['seed'],
                steps=settings['steps'],
                cfg_scale=settings['cfg_scale'],
                width=settings['width'],
                height=settings['height'],
                sampler=settings['sampler']
            )

        for resp in answers:
            for artifact in resp.artifacts:
                if check_artifact(artifact) == False:
                    global img
                    img = Image.open(io.BytesIO(artifact.binary))
                    response = serve_image(img)
                    response.headers.add("Access-Control-Allow-Origin", "*")
                    return response


# updates the prompt to use to retrieve the next image.
@app.route('/update_prompt', methods=["GET"])
def update_prompt():
    if request.method == "GET":
        args = request.args
        if args.get("prompt") == None:
            abort(400, "No prompt provided")
        else:
            settings["prompt"] = args.get("prompt")
            response = jsonify({
                "success": True
            })
            response.headers.add("Access-Control-Allow-Origin", "*")
            return response


@app.route("/debug", methods=["POST"])
def debug():
    if request.method == "POST":

        _img = decode_b64_image(request.form.get("image"))

        answers = stability_api.generate(
            prompt=settings['prompt'],
            seed=settings['seed'],
            steps=settings['steps'],
            cfg_scale=settings['cfg_scale'],
            width=settings['width'],
            height=settings['height'],
            sampler=settings['sampler'],
            init_image=_img
        )

        for resp in answers:
            for artifact in resp.artifacts:
                if check_artifact(artifact) == False:
                    global img
                    img = Image.open(io.BytesIO(artifact.binary))

                    response = serve_image(img)
                    response.headers.add("Access-Control-Allow-Origin", "*")
                    return response


# Takes an input image and uses it as the initial image for the prompt.
@app.route("/process", methods=["POST"])
def process_image():
    if request.method == "POST":
        file = decode_b64_image(request.form.get("image"))
        _img = Image.open(file.stream)

        answers = stability_api.generate(
            prompt=settings['prompt'],
            seed=settings['seed'],
            steps=settings['steps'],
            cfg_scale=settings['cfg_scale'],
            width=settings['width'],
            height=settings['height'],
            sampler=settings['sampler'],
            init_image=_img
        )

        for resp in answers:
            for artifact in resp.artifacts:
                if check_artifact(artifact) == False:
                    global img
                    img = Image.open(io.BytesIO(artifact.binary))

                    response = serve_image(img)
                    response.headers.add("Access-Control-Allow-Origin", "*")
                    return response


if __name__ == '__main__':
    app.run()

# To send a file is <response = send_file("static/test.jpg")>
