StablePainter
====
An experiment using StableDiffusion where you continuously build on top of each iteration of the generated image. 

Setup
===
* Add your Stability AI key to your environmental variables. 
* run `npm install` inside of the client folder. 
* Make sure the Pip package `stability_sdk` is installed.
* Make sure [Flask](https://flask.palletsprojects.com/en/2.2.x/) is installed.
* You can start the client development server by running `npm run dev`
* You can start the server by running the `app.py` file.

Tech
===
* [SolidJS](https://www.solidjs.com)
* [p5.js](https://p5js.org/)
* [StabilityAI's beta api](https://api.stability.ai/docs)